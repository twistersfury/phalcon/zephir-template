<?php
/**
 * @author    Phoenix <phoenix@twistersfury.com>
 * @license   proprietary
 * @copyright 2016 Twister's Fury
 */

//Only Set Save Path If Headers Not Sent (Indicates Automated Testing)
if (!headers_sent() && PHP_SESSION_NONE === session_status()) {
    ini_set("session.save_path", "/tmp");
}
