server {
    listen 80 default_server;
    listen [::]:80 default_server;

    server_name ${ENV_DOMAIN_NAME};
    root /var/www/public;

    #proxy_cache cache;
    #proxy_cache_valid 200 1s;

    index index.php;

    location @phalcon {
        rewrite ^(.*)$ /index.php?_url=$1;
    }

    location / {
        try_files $uri $uri/ @phalcon;
    }

    #Hot Update For Webpack Fix
    location ^/assets/(.*).hot-update.js* {
        return 301 $scheme://127.0.0.1:8080$request_uri;
    }

    location ^/assets/ {
        fastcgi_cache_valid 200 60m; # Only cache 200 responses, cache for 60 minutes
        fastcgi_cache_methods GET HEAD; # Only GET and HEAD methods apply
        add_header X-Fastcgi-Cache $upstream_cache_status;

        # If the file exists as a static file serve it directly without
        # running all the other rewrite tests on it
        #try_files $uri $uri/ @phalcon;
    }

    location ~ \.php {
        fastcgi_pass  upstream;
        fastcgi_index /index.php;

        include fastcgi_params;
        fastcgi_split_path_info       ^(.+\.php)(/.+)$;
        fastcgi_param APPLICATION_ENV development;
        fastcgi_param PATH_INFO       $fastcgi_path_info;
        fastcgi_param PATH_TRANSLATED $document_root$fastcgi_path_info;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
    }

    location ~ /\.ht {
        deny  all;
    }

    client_max_body_size 101m;
}
