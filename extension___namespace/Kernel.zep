namespace extension__namespace;

use Phalcon\Di;
use Phalcon\Di\DiInterface;
use Phalcon\Kernel as phKernel;
use Phalcon\Mvc\Application;

class Kernel extends phKernel
{
    private basePath = null;

    /**
     * Kernel Constructor
     *
     * Note: This is a minor implementation of the TwistersFury\Shared plugin. You can convert this over to extend the
     *          shared Kernel instead.
     */
    public function __construct(<DiInterface> di, string basePath)
    {
        Di::setDefault(di);

        Di::getDefault(di)->setShared("kernel", this);

        let this->basePath = rtrim(basePath, "/");
    }

    public function getApplication() -> <Application>
    {
        return new Application();
    }
}
