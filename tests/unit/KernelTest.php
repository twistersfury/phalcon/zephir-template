<?php

namespace extension__namespace\Tests\Unit;

use extension__namespace\Kernel;
use Codeception\Test\Unit;
use Phalcon\Di\FactoryDefault;
use Phalcon\Mvc\Application;

class KernelTest extends Unit
{
    /** @var Kernel */
    private $testSubject;

    /**
     * @var \UnitTester
     */
    protected $tester;

    public function _before()
    {
        $this->testSubject = new Kernel(new FactoryDefault(), "/base/path");
    }

    public function testGetApplication()
    {
        $this->assertInstanceOf(Application::class, $this->testSubject->getApplication());
    }
}
